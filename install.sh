#!/bin/bash

set -e

# Dont allow backup dir unbounded growth
mkdir -p  ~/.dotfiles-backup
BACKUPSIZE=$(du -c ~/.dotfiles-backup | tail -n1 | cut -f1)
if [ "$BACKUPSIZE" -gt "250000" ]; then
    echo "backup dir > 250M"
    exit 1
fi

# Setup dirs
TMPDIR=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
BACKUPDIR=~/.dotfiles-backup/$TMPDIR
mkdir -p $BACKUPDIR

# Make backups
set +e
mv ~/.vim $BACKUPDIR
mv ~/.vimrc $BACKUPDIR
mv ~/.oh-my-zsh $BACKUPDIR
mv ~/.zshrc $BACKUPDIR
mv ~/.gitconfig $BACKUPDIR
set -e

# Create symlinks
ln -s $PWD/.vimrc ~/.vimrc
ln -s $PWD/.zshrc ~/.zshrc
ln -s $PWD/.gitconfig ~/.gitconfig

# Setup Vim
git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim -i NONE -c VundleUpdate -c quitall

# Setup ZSH
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
