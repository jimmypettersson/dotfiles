# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

ZSH_THEME="maran"
HYPHEN_INSENSITIVE="true"

# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git jsontools docker golang zsh-syntax-highlighting)

# User configuration

export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# aliases
alias rsync='rsync -rlptoDvhP'

# Go stuff
export PATH=$PATH:/usr/local/go/bin
export GOPATH=$HOME/Development/go
export GOROOT=/usr/local/go
export PATH=$PATH:$GOPATH/bin
# alias cdgo='cd /home/jimmy/Development/go/src/github.com/slimmy/safeway'

# Set vim as default editor
export GIT_EDITOR=vim
export VISUAL=vim
export EDITOR=vim

# If we have no ssh-agent, find oldest still living agent and use that.
ssh-add -l >/dev/null 2>&1
if [[ $? -eq 2 ]]; then # ssh-add exists, but we have no agent
    unset SSH_AUTH_SOCK
    for socket in /tmp/ssh-*/agent.*(NU=Om); do
        SSH_AUTH_SOCK=$socket ssh-add -l >/dev/null 2>&1
        if [[ $? -lt 2 ]]; then # this agent is alive
            export SSH_AUTH_SOCK=$socket
            break
        fi
    done

    if [[ -z $SSH_AUTH_SOCK ]]; then # no agent, start one
        eval $(ssh-agent -s)
    fi
fi
